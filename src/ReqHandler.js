class RequestHandler {
  static socketUrl = "wss://ws.blockchain.info/inv";
  // static assignmentPath = configurations.assignmentPath;

  static subToHook(callback, handleDataStream, listenTo = '{"op":"unconfirmed_sub"}') {
    let socket = new WebSocket("wss://ws.blockchain.info/inv");
    callback("onlineIndicator", "Connecting...");
    socket.addEventListener("open", function(event) {
      callback("onlineIndicator", "Connected");
      socket.send(listenTo);
    });

    socket.addEventListener("message", function(event) {
      let eventData = event.data;
      handleDataStream(JSON.parse(eventData));
    });

    socket.addEventListener("close", function(event) {
      callback("onlineIndicator", "Disconnected");
    });
  }
}

export default RequestHandler;
