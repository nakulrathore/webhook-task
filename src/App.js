import React from "react";
import "./App.css";
import update from "immutability-helper";
import RequestHandler from "./ReqHandler";
import { cloneDeep } from "lodash";
import "../node_modules/react-linechart/dist/styles.css";

import { LineChart, PieChart } from 'react-chartkick'
import 'chart.js'

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      onlineIndicator: "disconnected",
      btcData: []
    };
  }

  handleChange(which, what) {
    let newState = update(this.state, {
      [which]: {
        $set: what
      }
    });
    this.setState(newState);
  }

  handleDataStream(btcObj) {
    let btcValue = btcObj["x"]["out"][0].value / 100000000;
    let newState = cloneDeep(this.state);

    let lengthOfBtcValues = newState.btcData.length;
    if (lengthOfBtcValues > 10) {
      newState.btcData = newState.btcData.slice(-1 * 10);
    }
    if(btcValue >= 1){
      newState.btcData.push(btcValue);
      this.setState(newState);
    }
  }

  componentDidMount() {
    RequestHandler.subToHook(this.handleChange.bind(this), this.handleDataStream.bind(this));
  }

  render() {

    let chartData = Object.assign({}, this.state.btcData);
    
    return (
      <section>
        <span className="indic">{this.state.onlineIndicator}</span>

        <div className="chart">
          {this.state.btcData.map(value => (
            <div key={value}>{value}</div>
          ))}


        </div>
        <LineChart data={chartData} />
      </section>
    );
  }
}

export default App;
